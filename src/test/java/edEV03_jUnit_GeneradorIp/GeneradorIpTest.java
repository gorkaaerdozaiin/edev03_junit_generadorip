package edEV03_jUnit_GeneradorIp;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class GeneradorIpTest {

	GeneradorIp ip = new GeneradorIp();
	
	@Test
	void testGenerarNumero() {
		for (int i = 0; i < 1000; i++) {
			assertTrue(ip.generarNumero(0, 255) >= 0 && ip.generarNumero(0, 255) <= 255);
		}
	}

	@Test
	void testGenerarIp() {
		String[] numeros = ip.generarIp().split("\\.");
		int numero1 = Integer.parseInt(numeros[0]);
		int numero4 = Integer.parseInt(numeros[3]);
		assertTrue(numero1 != 0 && numero4 != 0);
	}

}
