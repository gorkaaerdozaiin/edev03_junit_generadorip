package edEV03_jUnit_GeneradorIp;

import java.util.Random;

public class GeneradorIp {

	public int generarNumero(int min, int max) {
		 Random random = new Random();
		 int rn = random.nextInt((max - min) + 1) + min;
		 return rn;
	}
	
	public String generarIp() {
		String ip = "";
		for (int i = 1; i <= 4; i++) {
			ip += generarNumero(0, 255) + ".";
		}
		ip = ip.substring(0, ip.length() - 1);
		return ip;
	}
	
}
